require('dotenv').config()
const { MongoClient, ServerApiVersion, ObjectId } = require('mongodb');
const jwt = require('jsonwebtoken')
const express = require('express')
const app = express()
const cors = require("cors")
const port = process.env.PORT||4000
const stripe = require("stripe")(process.env.STRIPE_KEY);





app.use(express.json())
app.use(cors())



const uri = `mongodb+srv://${process.env.DB_NAME}:${process.env.DB_PASS}@cluster0.4mwwnz0.mongodb.net/?retryWrites=true&w=majority&appName=Cluster0`;

const client = new MongoClient(uri, {
  serverApi: {
    version: ServerApiVersion.v1,
    strict: true,
    deprecationErrors: true,
  }
});

async function run() {
  try {

    const contestCollection = client.db("ContestMaster").collection("contest")
    const userCollection = client.db("ContestMaster").collection("users")
    const participateCollection = client.db("ContestMaster").collection("participates")
    const paymentCollection = client.db("ContestMaster").collection('payments')
    const contestResultCollection = client.db("ContestMaster").collection('contestResult')
// ----------------------------------------
// jwt
app.post('/jwt',async(req,res)=>{
  const user=req.body
  const token = jwt?.sign(user,process.env.SECREAT_TOKEN,{expiresIn:'7d'})
  res.send(token)
})
  
//middleWire
const verifyToken = async(req,res,next)=>{
  const token = req?.headers?.authorization?.split(' ')[1]
  if(!token){
    res.status(401).send({message:'Unauthorized access'})
  }
  jwt.verify(token,process.env.SECREAT_TOKEN,(err,decode)=>{
    if(err){
      console.log('getting error in middle wire');
      return res.status(401).send({message:'Unauthorized access'})
    }
    console.log('token in middle wire->',token);

    req.decode=decode
    next()

  })

}

//admin verify api
app.get('/user/admin/:email',async(req,res)=>{
  const email = req.params.email
  console.log(email);

  const query ={email:email}
  const user = await userCollection.findOne(query)
  let admin = false;
  if(user){
    admin = user?.role==="Admin"
  }
  res.send({admin})

})

//ADMIN MIDDLE WIRE
const verifyAdmin = async(req,res,send)=>{
  const emai = req.decode.email
  const query ={email:email}
  const user = await userCollection.findOne(query)
  const isAdmin = user.role==="Admin"
  if(!admin){
    res.status(401).send({message:"Forbidden access"})
  }
  next()
}


    // Payments
    app.post("/create-payment-intent",verifyToken,async (req, res) => {
      const {price} = req.body;
      const amount=parseInt(price*100)
      const paymentIntent = await stripe.paymentIntents.create({
        amount: amount,
        currency: "usd",
        payment_method_types:['card']
      });

      res.send({
        clientSecret: paymentIntent.client_secret,
      });
    });
    //participates and payments collection
      app.post('/payment',verifyToken,async(req,res)=>{
        const data = req.body;
        const id = data?.userContestId
        const query = {_id:new ObjectId(id)}
        const update={
          $set:{
            paymentApproval:'pending'
          }
        }
   
        if(data.transactionId){ 
          const upDoc = await participateCollection.updateOne(query,update)
          
          if(upDoc.modifiedCount>0){
            const result = await paymentCollection.insertOne(data)
            res.send(result)
          }
        }
    
     
    })
    //payment collection get
      //participates get
      app.get('/participates-data',verifyToken,async(req,res)=>{
        const data = await paymentCollection.find().toArray()
        res.send(data)
     }) 

     //payment get according to id
       app.get('/payment-collection/:email',async(req,res)=>{
        const email = req.params.email;
        const query = {"publisher.email":email}
        const result = await paymentCollection.find(query).toArray()
        res.send(result)
       })
//--------- data update in some collection-----------------------
       app.patch('/payment-status/:userContestId',async(req,res)=>{
        const userContestId = req.params.userContestId
        const query ={userContestId:userContestId}
        const queryTwo = {_id:new ObjectId(userContestId)}
        const {contestId} = req.body.collection
        const queryThree = {_id:new ObjectId(contestId)}
        console.log(req.body,'6666');
        const status={
          $set:{
            paymentApproval:'Approve'
          }
        }
        const options={upsert:true}

        const contestQtyUpdate = {
          $inc:{
            qty:1
          }
        }

        console.log(contestId,'---------->');

          const dataThree = await contestCollection.updateOne(queryThree,contestQtyUpdate)
          const dataTwo = await participateCollection.updateOne(queryTwo,status,options)
          const data = await paymentCollection.updateOne(query,status)
              res.send([data,dataTwo,'qty update---->',dataThree])
        
        
       })
  
//--------------------------------------------------------------------------------------------
  // participate-collection
    app.post('/participate-collection',async(req,res)=>{
      const data = req.body;
      const contestId =  data.contestId
      const participateEmail = data.participateEmail
      const query = {participateEmail,contestId}
      const find = await participateCollection.findOne(query)
      if(find){
       return res.status(409).send({message:"Contest Already in card"})
      }
    const result = await participateCollection.insertOne(data)
    res.send(result)
    })
//participatedContestResult post
    app.post('/contestTaskSubmit',verifyToken,async(req,res)=>{
      const {submittedTask} = req.body;
      const result = await contestResultCollection.insertOne(submittedTask)
      res.send(result)
      console.log(submittedTask,'------------->');
    })
 ///participatedContestResult get
 app.get('/contest-result',async(req,res)=>{
  const data = await contestResultCollection.find().toArray()
  res.send(data)
 })
 //patcjh result data
 app.patch('/result',async(req,res)=>{
  const {id,result,contestName} = req.body

  const query = {_id:new ObjectId(id)}

  
  
  try {
    const data = await contestResultCollection.updateOne(
      query,
      {$set:{result:result}}
      )
    console.log(data);

   res.send(data)
  } catch (error) {
    console.log(error);
    res.send(error)
  }

  ;
 })


  //participated email wise data
  app.get('/participated-user/:email',async(req,res)=>{
    const email = req.params.email;
    const query = {participateEmail:email}
    const result = await participateCollection.find(query).toArray()
    res.send(result)
  })
// --------------------------------------------------------------------



// -----------------------------------------------------------
  // single contest get
  app.get('/contestDetails/:id',verifyToken,async (req,res)=>{
    const id = req.params.id
    const query = {_id:new ObjectId(id)}
    const data = await contestCollection.findOne(query)
    res.send(data)
    // console.log(data,'----------->');
  })
  // contest post
  app.post("/contest",verifyToken,async(req,res)=>{
    const data = req.body
    const result = await contestCollection.insertOne(data)
    res.send(result)
    
  })
  // contest get
  app.get("/contest",async(req,res)=>{
  const data = await contestCollection.find().toArray()
  res.send(data)
  })
  //singleContest get
  app.get('/contest/:email',verifyToken,async(req,res)=>{
    const email = req.params.email;
    const query = {"publisher.email":email}
    const result = await contestCollection.find(query).toArray()
    res.send(result)

  })
  //contest update
  app.put('/contest/:id',verifyToken,async(req,res)=>{
    const id = req.params.id
    const query ={_id:new ObjectId(id)}
    const updateContest = req.body;
    const options = {upsert:true}
    const contest = {
      $set:{
        contestName:updateContest.contestName,
        contestDescription:updateContest.contestDescription,
        prize:updateContest.prize,
        price:updateContest.price,
        contestType:updateContest.contestType,
        taskSubmissionInstructions:updateContest.taskSubmissionInstructions,
        endDate:updateContest.endDate,
        image:updateContest.image,
      }
    }
    const result =  await contestCollection.updateOne(query,contest,options)
    res.send(result)
  })
  //contest delete
  app.delete('/contest/:id',verifyToken,async(req,res)=>{
    const id = req.params.id;
    const query = {_id:new ObjectId(id)}
    const result = await contestCollection.deleteOne(query)
    res.send(result)
    
  })
// ----------------------------------------------------------
    



//----save use in db-------
    app.post("/user-data",async(req,res)=>{
      const user=req.body
      const {email} = user

      const exitUser = await userCollection.findOne({email})
      if(exitUser){
        res.status(400).send('User profile already saved')
        return
      }
      const data = await userCollection.insertOne(user)
      res.send(data)
      
    })

//-----get user from db-----------------------
    app.get("/user-data/:email",async(req,res)=>{
      const data = req.params.email
      const query = {email:data }
      const result = await userCollection.findOne(query)
      res.send(result)
    })
    
// DbUser Get
  app.get('/db-user',verifyToken,async(req,res)=>{
    const data = await userCollection.find().toArray()
    res.send (data)
  })
// dbUser Patch
  app.patch('/user-role-update',verifyToken,async(req,res)=>{
    const {email,role} = req.body
    const query = {email:email}
    const update = {$set:{ role:role }}
    const data = await userCollection.updateOne(query,update)
    res.send(data)



  })
//dbuser deleted
app.delete('/user-delete',verifyToken,async(req,res)=>{
  const {email} = req.query
  const query = {email:email}
  const data = await userCollection.deleteOne(query)
  res.send(data)


    console.log(email,'email.role');

}) 

// Counter
app.get('/counter',async(req,res)=>{
  const participates = await participateCollection.estimatedDocumentCount()
  const contest = await contestCollection.estimatedDocumentCount()
  const totalQty = await contestCollection.aggregate([
   {
    $group:{
      _id:null,
      totalQty:{$sum:'$qty'}
    }
   }
  ]).toArray()
  const qty = totalQty.length > 0 ? totalQty[0].totalQty : 0;

  console.log(qty,'qty');

  res.send({qty,participates,contest})
})








    
    console.log("Pinged your deployment. You successfully connected to MongoDB!");
  } finally {


  }
}
run().catch(console.dir);











app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.listen(port, () => {
  console.log(`Contest Management running ${port}`)
})